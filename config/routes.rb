Rails.application.routes.draw do
  devise_for :users
  resources :tags
  resources :games
  resources :miniatures
  root to: "home#index", as: 'home'
  get '/dashboard', to: 'home#dashboard', as: 'dashboard'
  get '/collection/:game', to: 'miniatures#by_user_for_game', as: 'user_miniatures'
end
