class AddGameIdToMiniatures < ActiveRecord::Migration
  def change
    add_reference :miniatures, :game, index: true
  end
end
