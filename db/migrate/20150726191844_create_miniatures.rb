class CreateMiniatures < ActiveRecord::Migration
  def change
    create_table :miniatures do |t|
      t.string :name
      t.integer :count
      t.text :description
      t.belongs_to :user

      t.timestamps null: false
    end
  end
end
