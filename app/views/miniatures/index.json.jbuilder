json.array!(@miniatures) do |miniature|
  json.extract! miniature, :id, :name, :count, :description
  json.url miniature_url(miniature, format: :json)
end
