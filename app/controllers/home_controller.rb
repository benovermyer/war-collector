class HomeController < ApplicationController
  def index
  end

  def dashboard
    @games = Game.all
  end
end
