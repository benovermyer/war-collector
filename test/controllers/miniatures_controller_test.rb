require 'test_helper'

class MiniaturesControllerTest < ActionController::TestCase
  setup do
    @miniature = miniatures(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:miniatures)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create miniature" do
    assert_difference('Miniature.count') do
      post :create, miniature: { count: @miniature.count, description: @miniature.description, name: @miniature.name }
    end

    assert_redirected_to miniature_path(assigns(:miniature))
  end

  test "should show miniature" do
    get :show, id: @miniature
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @miniature
    assert_response :success
  end

  test "should update miniature" do
    patch :update, id: @miniature, miniature: { count: @miniature.count, description: @miniature.description, name: @miniature.name }
    assert_redirected_to miniature_path(assigns(:miniature))
  end

  test "should destroy miniature" do
    assert_difference('Miniature.count', -1) do
      delete :destroy, id: @miniature
    end

    assert_redirected_to miniatures_path
  end
end
